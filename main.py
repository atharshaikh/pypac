import sys

import requests

from pkg import Package
from table import show_package_details

base_url = "https://www.archlinux.org/packages/search/json/"

if len(sys.argv) < 2:
    print("Error no package to search for")
    sys.exit(-1)
else:
    name = sys.argv[1]

req = requests.get(base_url, params={"q": name})
data = req.json()["results"]
packages = []
for pkg in data:
    package = Package(
        pkg["pkgname"],
        pkg["pkgdesc"],
        pkg["pkgver"],
        pkg["repo"],
        pkg["depends"],
        pkg["optdepends"],
        pkg["checkdepends"],
        pkg["conflicts"],
    )
    packages.append(package)

show_package_details(packages)
