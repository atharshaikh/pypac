from rich import box
from rich.console import Console
from rich.table import Table

from pkg import Package


def show_package_details(packages: list[Package]):
    table = Table(title="Package Description", box=box.ROUNDED)

    table.add_column("Sr. No", justify="right", style="color(100)")
    table.add_column("Package Name", justify="center", style="cyan")
    table.add_column("Repository", style="red")
    table.add_column("Description", style="magenta")
    table.add_column("Depends On", justify="center", style="green")

    # print(pkg)
    for i, pkg in enumerate(packages):
        table.add_row(
            str(i + 1), pkg.pkgname, pkg.repo, pkg.pkgdesc, ", ".join(pkg.depends)
        )
    console = Console()
    console.print(table)
