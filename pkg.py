from dataclasses import dataclass


@dataclass
class Package:
    pkgname: str
    pkgdesc: str
    pkgver: str
    repo: str
    depends: list[str]
    optdepends: list[str]
    checkdepends: list[str]
    conflicts: list[str]
