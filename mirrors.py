def get_mirrors():
    with open("/etc/pacman.d/mirrorlist", mode="r") as mirrorfile:
        mirrors = (
            i.split("=")[1].strip()
            for i in mirrorfile.readlines()
            if i.startswith("Server")
        )
    return mirrors


if __name__ == "__main__":
    mirror_list = get_mirrors()
    for mirror in mirror_list:
        print(mirror)
