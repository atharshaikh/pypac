# Checks if the user is running Arch Linux, and has a valid version of pacman installed
import subprocess
import sys


def check_pacman_install():
    result = subprocess.run(
        ["pacman -V"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True
    )
    if not result.returncode == 0:
        sys.exit("Could not find pacman")


if __name__ == "__main__":
    check_pacman_install()
