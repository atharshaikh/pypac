# PyPac

A WIP python wrapper for pacman and aur.


## How to run?

* Create a new virtual environment
* `pip install -r requirements.txt`
* Search for a package on the official repositories:
    `python main.py "package-name"`


![Sample output](images/file1.png)

### TODO
* Add check to verify if running Arch Linux, and if pacman version is recent enough
* Download metadata from mirrors, and update them if old.
* Option to either search from:
  * downloaded metadata (_default_)
  * arch linux website
* Switch to using argparse (or another flag parser)
